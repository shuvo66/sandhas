(function ($) {
	"use strict";

    $(document).ready(function() {
        
        // Our Menu Sliding Tab
        $('.menu-slider').owlCarousel({
            items: 1,
            loop:0,
            margin:10,
            nav:false,
            dots:true,
            autoplay: true,
            mouseDrag: false,
            touchDrag: false,
            dotsContainer: '.dotsCont'
        });

        $('.dotsCont .owl-dot').click(function () {
          $('.menu-slider').trigger('to.owl.carousel', [$(this).index(), 300]);
        });

        // Testimonial Slider
        $(".testimonial-slides").owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            autoplay: true,
            animateOut: 'slideOutDown',
            animateIn: 'flipInX',
            loop: true,
            navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
            mouseDrag: false,
            touchDrag: false,
        });

        // Popup Video
        $('.ct-video-popup').magnificPopup({
            //disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
        
     
    
        
  
     }); //.document/ready

    $(window).on('load', function(){
        
        

    });

}(jQuery));